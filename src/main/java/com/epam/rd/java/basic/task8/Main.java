package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.model.Flower;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.util.Comparator;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
				// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.readDataXML();
		// PLACE YOUR CODE HERE

		// sort (case 1)
		domController.flowers.getFlowers().sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getName().compareTo(o2.getName());
			}
		});
		// PLACE YOUR CODE HERE
		System.out.println(domController.flowers.getFlowers());
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
              domController.createDataXML(outputXmlFile, domController.flowers);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);

		File file = new File(xmlFileName);

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(file,saxController);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}


		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		saxController.flowers.getFlowers().sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getGrowingTips().getWatering().getContent().compareTo(o2.getGrowingTips().getWatering().getContent());
			}
		});
		// PLACE YOUR CODE HERE
		System.out.println("saxFlowers" + saxController.flowers.getFlowers());
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		domController.createDataXML(outputXmlFile, saxController.flowers);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.readDataXML();
		// PLACE YOUR CODE HERE
		staxController.flowers.getFlowers().sort(new Comparator<Flower>() {
			@Override
			public int compare(Flower o1, Flower o2) {
				return o1.getGrowingTips().getTempreture().getContent().compareTo(o2.getGrowingTips().getTempreture().getContent());
			}
		});
		// sort  (case 3)
		// PLACE YOUR CODE HERE



		// save
		outputXmlFile = "output.stax.xml";
		domController.createDataXML(outputXmlFile, staxController.flowers);
		// PLACE YOUR CODE HERE
	}

}
