package com.epam.rd.java.basic.task8.model;

public class AveLenFlower {
    String measure;
    String content;

    public AveLenFlower(String measure, String content) {
        this.measure = measure;
        this.content = content;
    }

    public AveLenFlower() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "measure='" + measure + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
