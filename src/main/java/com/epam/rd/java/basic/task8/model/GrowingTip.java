package com.epam.rd.java.basic.task8.model;

import java.util.List;

public class GrowingTip {
    Tempreture tempreture;
    Lighting lighting;
    Watering watering;

    public GrowingTip() {
    }

    public GrowingTip(Tempreture tempreture, Lighting lighting, Watering watering) {
        this.tempreture = tempreture;
        this.lighting = lighting;
        this.watering = watering;
    }

    public Tempreture getTempreture() {
        return tempreture;
    }

    public void setTempreture(Tempreture tempreture) {
        this.tempreture = tempreture;
    }

    public Lighting getLighting() {
        return lighting;
    }

    public void setLighting(Lighting lighting) {
        this.lighting = lighting;
    }

    public Watering getWatering() {
        return watering;
    }

    public void setWatering(Watering watering) {
        this.watering = watering;
    }

    @Override
    public String toString() {
        return "GrowingTip{" +
                "tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}
