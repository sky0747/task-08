package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.events.Attribute;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    static public List<Flower> flowerList = new ArrayList<>();
    static public Flowers flowers = new Flowers(flowerList);
    Flower flower = null;
    VisualParameter visualParameter = null;
    GrowingTip growingTip = null;
    AveLenFlower aveLenFlower = null;
    Lighting lighting = null;
    Watering watering = null;
    Tempreture tempreture = null;
    boolean isName, isSoil, isOrigin, isSteamColour, isLeafColour, isMeasureAveLenFlower, isMeasureTempreture,
            isLightRequiring, isMeasureWatering, isMultiplying;
    String contentAveLenFlower = null, contentTempreture = null,
            contentWatering = null, contentLight = null;

    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch (qName) {
            case "flower":
                flower = new Flower();
                break;
            case "name":
                isName = true;
                break;
            case "soil":
                isSoil = true;
                break;
            case "origin":
                isOrigin = true;
                break;
            case "visualParameters":
                visualParameter = new VisualParameter();
                break;
            case "stemColour":
                isSteamColour = true;
                break;
            case "leafColour":
                isLeafColour = true;
                break;
            case "aveLenFlower":
                aveLenFlower = new AveLenFlower();
                isMeasureAveLenFlower = true;
                contentAveLenFlower = attributes.getValue("measure");
                break;
            case "growingTips":
                growingTip = new GrowingTip();
                break;
            case "tempreture":
                tempreture = new Tempreture();
                contentTempreture = attributes.getValue("measure");
                isMeasureTempreture = true;
                break;
            case "lighting":
                lighting = new Lighting();
                contentLight = attributes.getValue("lightRequiring");
                isLightRequiring = true;
            case "watering":
                watering = new Watering();
                contentWatering = attributes.getValue("measure");
                isMeasureWatering = true;
                break;
            case "multiplying":
                isMultiplying = true;
                break;
        }
    }

    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case "flower":
                visualParameter.setAveLenFlower(aveLenFlower);
                growingTip.setLighting(lighting);
                growingTip.setTempreture(tempreture);
                growingTip.setWatering(watering);
                flower.setGrowingTips(growingTip);
                flower.setVisualParameters(visualParameter);
                flowerList.add(flower);
                System.out.println("flowerList " + flower);
                break;
//            case "name":
//                isName = true;
//                break;
        }
    }

    @Override
    public void characters(char ch[], int start, int lenght) {
        if (isName) {
            flower.setName(new String(ch, start, lenght));
            isName = false;
        } else if (isSoil) {
            flower.setSoil(new String(ch, start, lenght));
            isSoil = false;
        } else if (isOrigin) {
            flower.setOrigin(new String(ch, start, lenght));
            isOrigin = false;
        } else if (isMultiplying) {
            flower.setMultiplying(new String(ch, start, lenght));
            isMultiplying = false;
        } else if (isMeasureWatering) {
            //watering.setContent(new String(ch, start, lenght));
            watering.setContent(new String(ch, start, lenght));
            watering.setMeasure(contentWatering);
            isMeasureWatering = false;
        } else if (isSteamColour) {
            visualParameter.setStemColour(new String(ch, start, lenght));
            isSteamColour = false;
        } else if (isLeafColour) {
            visualParameter.setLeafColour(new String(ch, start, lenght));
            isLeafColour = false;
        } else if (isMeasureAveLenFlower) {
            aveLenFlower.setContent(new String(ch, start, lenght));
            aveLenFlower.setMeasure(contentAveLenFlower);
            isMeasureAveLenFlower = false;
        } else if (isMeasureTempreture) {
            tempreture.setContent(new String(ch, start, lenght));
            tempreture.setMeasure(contentTempreture);
            isMeasureTempreture = false;
        } else if (isLightRequiring) {
            lighting.setLightRequiring(contentLight);
            isLightRequiring = false;
        }
    }


}