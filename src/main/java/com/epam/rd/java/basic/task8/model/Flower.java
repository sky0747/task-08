package com.epam.rd.java.basic.task8.model;



public class Flower {
    String name;
    String soil;
    String origin;
    VisualParameter visualParameters;
    GrowingTip growingTips;
    String multiplying;

    public Flower() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameter getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameter visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTip getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTip growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public Flower(String name, String soil, String origin, VisualParameter visualParameters, GrowingTip growingTips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
