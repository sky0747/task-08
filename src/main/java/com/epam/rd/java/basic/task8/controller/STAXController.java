package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Main;
import com.epam.rd.java.basic.task8.model.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    static List<Flower> flowerList = new ArrayList<>();
    static public Flowers flowers = new Flowers(flowerList);
    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    // PLACE YOUR CODE HERE

    public void readDataXML() {
        Flower flower = null;
        VisualParameter visualParameter = null;
        GrowingTip growingTip = null;
        AveLenFlower aveLenFlower = null;
        Lighting lighting = null;
        Watering watering = null;
        Tempreture tempreture = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
            while (reader.hasNext()) {
                XMLEvent xmlEvent = reader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();

                    switch (startElement.getName().getLocalPart()) {
                        case "flower":
                            flower = new Flower();
                            break;
                        case "name":
                            xmlEvent = reader.nextEvent();
                            flower.setName(xmlEvent.asCharacters().getData());
                            break;
                        case "soil":
                            xmlEvent = reader.nextEvent();
                            flower.setSoil(xmlEvent.asCharacters().getData());
                            break;
                        case "origin":
                            xmlEvent = reader.nextEvent();
                            flower.setOrigin(xmlEvent.asCharacters().getData());
                            break;
                        case "visualParameters":
                            visualParameter = new VisualParameter();
                            break;
                        case "growingTips":
                            growingTip = new GrowingTip();
                            break;
                        case "tempreture":
                            tempreture = new Tempreture();
                            Attribute tempretureAttribute = startElement.getAttributeByName(new QName("measure"));
                            tempreture.setMeasure(tempretureAttribute.getValue());
                            xmlEvent = reader.nextEvent();
                            tempreture.setContent(xmlEvent.asCharacters().getData());
                            break;
                        case "watering":
                            watering = new Watering();
                            Attribute wateringAttribute = startElement.getAttributeByName(new QName("measure"));
                            watering.setMeasure(wateringAttribute.getValue());
                            xmlEvent = reader.nextEvent();
                            watering.setContent(xmlEvent.asCharacters().getData());
                            break;
                        case "lighting":
                            lighting = new Lighting();
                            Attribute lightingAttribute = startElement.getAttributeByName(new QName("lightRequiring"));
                            lighting.setLightRequiring(lightingAttribute.getValue());
                            break;
                        case "stemColour":
                            xmlEvent = reader.nextEvent();
                            visualParameter.setStemColour(xmlEvent.asCharacters().getData());
                            break;
                        case "leafColour":
                            xmlEvent = reader.nextEvent();
                            visualParameter.setLeafColour(xmlEvent.asCharacters().getData());
                            break;
                        case "aveLenFlower":
                            aveLenFlower = new AveLenFlower();
                            Attribute aveLenFlowerAttribute = startElement.getAttributeByName(new QName("measure"));
                            aveLenFlower.setMeasure(aveLenFlowerAttribute.getValue());
                            xmlEvent = reader.nextEvent();
                            aveLenFlower.setContent(xmlEvent.asCharacters().getData());
                            break;
                        case "multiplying":
                            xmlEvent = reader.nextEvent();
                            flower.setMultiplying(xmlEvent.asCharacters().getData());
                            break;
                    }


                }
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("flower")) {
                        growingTip.setWatering(watering);
                        growingTip.setLighting(lighting);
                        growingTip.setTempreture(tempreture);
                        flower.setGrowingTips(growingTip);
                        visualParameter.setAveLenFlower(aveLenFlower);
                        flower.setVisualParameters(visualParameter);
                        flowerList.add(flower);
                    }
                }

            }


        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("flowerList" + flowerList);
    }
/*
private static List<Student> parseXMLfile(String fileName) {
        List<Student> studentsList = new ArrayList<>();
        Student student = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        try {
            // инициализируем reader и скармливаем ему xml файл
            XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));
            // проходим по всем элементам xml файла
            while (reader.hasNext()) {
                // получаем событие (элемент) и разбираем его по атрибутам
                XMLEvent xmlEvent = reader.nextEvent();
                if (xmlEvent.isStartElement()) {
                    StartElement startElement = xmlEvent.asStartElement();
                    if (startElement.getName().getLocalPart().equals("Student")) {
                        student = new Student();
                        // Получаем атрибут id для каждого элемента Student
                        Attribute idAttr = startElement.getAttributeByName(new QName("id"));
                        if (idAttr != null) {
                            student.setId(Integer.parseInt(idAttr.getValue()));
                        }
                    } else if (startElement.getName().getLocalPart().equals("age")) {
                        xmlEvent = reader.nextEvent();
                        student.setAge(Integer.parseInt(xmlEvent.asCharacters().getData()));
                    } else if (startElement.getName().getLocalPart().equals("name")) {
                        xmlEvent = reader.nextEvent();
                        student.setName(xmlEvent.asCharacters().getData());
                    } else if (startElement.getName().getLocalPart().equals("language")) {
                        xmlEvent = reader.nextEvent();
                        student.setLanguage(xmlEvent.asCharacters().getData());
                    }
                }
                // если цикл дошел до закрывающего элемента Student,
                // то добавляем считанного из файла студента в список
                if (xmlEvent.isEndElement()) {
                    EndElement endElement = xmlEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("Student")) {
                        studentsList.add(student);
                    }
                }
            }

        } catch (FileNotFoundException | XMLStreamException exc) {
            exc.printStackTrace();
        }
        return studentsList;
    }

}
 */

//    public static void main(String[] args) {
//        STAXController staxC = new STAXController("input.xml");
//        staxC.readDataXML("input.xml");
//
//    }
}