package com.epam.rd.java.basic.task8.model;

import java.util.List;

public class Flowers {
    List<Flower> flowers;

    public Flowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public Flowers() {
    }



    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "flowers=" + flowers +
                '}';
    }
}
