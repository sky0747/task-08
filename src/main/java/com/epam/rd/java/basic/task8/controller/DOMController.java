package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.model.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;
    static public Flowers flowers = new Flowers();
    Node flowerNode = null;
    Node visualNode = null;
    Node growingNode = null;

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void readDataXML() {
        //File file = new File("input.xml");
        File file = new File(xmlFileName);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document doc = null;
        try {
            doc = dbf.newDocumentBuilder().parse(file);
        } catch (Exception e) {
            System.out.println("Open DOM-parsing error" + e.toString());
            return;
        }
        Node flowersNode = doc.getFirstChild();
        NodeList flowersChilds = flowersNode.getChildNodes();

        NodeList elementChilds = null;
        List<Flower> flowersList = new ArrayList<>();
        String name = null;
        String soil = null;
        String origin = null;
        List<VisualParameter> visualParameters = new ArrayList<>();
        List<GrowingTip> growingTips = new ArrayList<>();
        String multiplying = null;
        Flower flower = new Flower();

        for (int i = 0; i < flowersChilds.getLength(); i++) {
            if (flowersChilds.item(i).getNodeType() != Node.ELEMENT_NODE) continue;
            //  System.out.println("NodeStart " + flowersChilds.item(i).getNodeName());
            switch (flowersChilds.item(i).getNodeName()) {
                case "flower":
                    flowerNode = flowersChilds.item(i);
                    break;
            }

            NodeList flowerChilds = flowerNode.getChildNodes();
            flower = new Flower();
            VisualParameter visualParameter = new VisualParameter();
            NodeList visualChilds = null;
            NodeList grownChilds = null;


            for (int j = 0; j < flowerChilds.getLength(); j++) {
                if (flowerChilds.item(j).getNodeType() != Node.ELEMENT_NODE) continue;

                switch (flowerChilds.item(j).getNodeName()) {
                    case "name":
                        name = flowerChilds.item(j).getTextContent();
                        flower.setName(name);
                        break;
                    case "soil":
                        soil = flowerChilds.item(j).getTextContent();
                        flower.setSoil(soil);
                        break;
                    case "origin":
                        origin = flowerChilds.item(j).getTextContent();
                        flower.setOrigin(origin);
                        break;
                    case "multiplying":
                        multiplying = flowerChilds.item(j).getTextContent();
                        flower.setMultiplying(multiplying);
                        break;
                    case "visualParameters":
                        visualNode = flowerChilds.item(j);
                        visualChilds = visualNode.getChildNodes();
                        //List<VisualParameter> vList = new ArrayList<>();
                        //vList.add(methodList(visualChilds));
                        flower.setVisualParameters(methodList(visualChilds));
                        break;
                    case "growingTips":
                        growingNode = flowerChilds.item(j);
                        grownChilds = growingNode.getChildNodes();
                        //List<GrowingTip> growList = new ArrayList<>();
                        //growList.add(methodListGrow(grownChilds));
                        flower.setGrowingTips(methodListGrow(grownChilds));
                        //  methodList(grownChilds);
                        break;

                }

            }
            if (flower != null) {
                flowersList.add(flower);
                System.out.println(flower);
            }


        }
        if (flowersList.size() > 0) {
            flowers = new Flowers(flowersList);
            System.out.println("flowers " + flowers);
        }

        if (flowerNode == null) return;
    }


    static VisualParameter methodList(NodeList visualChilds) {
        VisualParameter visualParameter = new VisualParameter();
        for (int j = 0; j < visualChilds.getLength(); j++) {
            if (visualChilds.item(j).getNodeType() != Node.ELEMENT_NODE) continue;

            switch (visualChilds.item(j).getNodeName()) {
                case "stemColour":
                    visualParameter.setStemColour(visualChilds.item(j).getTextContent());
                    break;
                case "leafColour":
                    visualParameter.setLeafColour(visualChilds.item(j).getTextContent());
                    break;
                case "aveLenFlower":
                    Node aveNode = visualChilds.item(j);
                    NodeList aveChilds = aveNode.getChildNodes();
                    AveLenFlower aveLenFlower = new AveLenFlower();
                    aveLenFlower.setContent(visualChilds.item(j).getTextContent());
                    NamedNodeMap aveAtrib = visualChilds.item(j).getAttributes();
                    aveLenFlower.setMeasure(aveAtrib.getNamedItem("measure").getNodeValue());
                    //List<AveLenFlower> avenFlowers = new ArrayList<>();
                    // avenFlowers.add(aveLenFlower);
                    visualParameter.setAveLenFlower(aveLenFlower);
                    break;
            }

        }
        return visualParameter;
    }


    static GrowingTip methodListGrow(NodeList visualChilds) {
        GrowingTip growingTip = new GrowingTip();
        for (int j = 0; j < visualChilds.getLength(); j++) {
            if (visualChilds.item(j).getNodeType() != Node.ELEMENT_NODE) continue;
            //System.out.println("visualChilds.item(j).getNodeName() " + visualChilds.item(j).getNodeName());
            switch (visualChilds.item(j).getNodeName()) {
                case "tempreture":
                    Node tempretureNode = visualChilds.item(j);
                    // NodeList wateringChilds = wateringNode.getChildNodes();
                    Tempreture tempreture = new Tempreture();
                    tempreture.setContent(visualChilds.item(j).getTextContent());
                    NamedNodeMap tempretureAtrib = visualChilds.item(j).getAttributes();
                    tempreture.setMeasure(tempretureAtrib.getNamedItem("measure").getNodeValue());
                    //List<Tempreture> tempreturesList = new ArrayList<>();
                    //tempreturesList.add(tempreture);
                    growingTip.setTempreture(tempreture);
                    break;
                case "lighting":
                    Node lightingNode = visualChilds.item(j);
                    Lighting lighting = new Lighting();
                    NamedNodeMap lightingAtrib = visualChilds.item(j).getAttributes();
                    lighting.setLightRequiring(lightingAtrib.getNamedItem("lightRequiring").getNodeValue());
                    //List<Lighting> lightingList = new ArrayList<>();
                    //lightingList.add(lighting);
                    growingTip.setLighting(lighting);
                    break;
                case "watering":
                    Node wateringNode = visualChilds.item(j);
                    //NodeList wateringChilds = wateringNode.getChildNodes();
                    Watering watering = new Watering();
                    watering.setContent(visualChilds.item(j).getTextContent());
                    NamedNodeMap wateringAtrib = visualChilds.item(j).getAttributes();
                    watering.setMeasure(wateringAtrib.getNamedItem("measure").getNodeValue());
                    //List<Watering> wateringList = new ArrayList<>();
                    //wateringList.add(watering);
                    growingTip.setWatering(watering);
                    break;
            }

        }
        return growingTip;
    }


    public void createDataXML(String nameFile, Flowers flowersForFile) {
        List<Flower> myFlowers = new ArrayList();
        if (flowersForFile == null) return;

        myFlowers = flowersForFile.getFlowers();
        System.out.println("flowers " + flowersForFile);

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();

            // создаем пустой объект Document, в котором будем
            // создавать наш xml-файл
            Document doc = builder.newDocument();
            // создаем корневой элемент
            Element rootElement = doc.createElement("flowers");
            //   doc.createElementNS("http://www.nure.ua", "flowers");
            rootElement.setAttribute("xmlns", "http://www.nure.ua");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        //    rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema");
            rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");

            // добавляем корневой элемент в объект Document
            doc.appendChild(rootElement);
            Element flowerXml = null;
            System.out.println("myFlowers " + myFlowers);
            for (Flower flower : myFlowers) {

                flowerXml = null;

                flowerXml = doc.createElement("flower");
                rootElement.appendChild(flowerXml);

                Element name = doc.createElement("name");
                name.setTextContent(flower.getName());
                flowerXml.appendChild(name);

                Element soil = doc.createElement("soil");
                soil.setTextContent(flower.getSoil());
                flowerXml.appendChild(soil);

                Element origin = doc.createElement("origin");
                origin.setTextContent(flower.getOrigin());
                flowerXml.appendChild(origin);


                Element visualParameters = doc.createElement("visualParameters");

                Element stemColour = doc.createElement("stemColour");
                stemColour.setTextContent(flower.getVisualParameters().getStemColour());
                visualParameters.appendChild(stemColour);

                Element leafColour = doc.createElement("leafColour");
                leafColour.setTextContent(flower.getVisualParameters().getLeafColour());
                visualParameters.appendChild(leafColour);

                Element aveLenFlower = doc.createElement("aveLenFlower");
                aveLenFlower.setAttribute("measure", flower.getVisualParameters().getAveLenFlower().getMeasure());
                aveLenFlower.setTextContent(flower.getVisualParameters().getAveLenFlower().getContent());
                visualParameters.appendChild(aveLenFlower);

                flowerXml.appendChild(visualParameters);

                /**/
                Element growingTips = doc.createElement("growingTips");

                Element tempreture = doc.createElement("tempreture");
                tempreture.setAttribute("measure", flower.getGrowingTips().getTempreture().getMeasure());
                tempreture.setTextContent(flower.getGrowingTips().getTempreture().getContent());
                growingTips.appendChild(tempreture);

                Element lighting = doc.createElement("lighting");
                lighting.setAttribute("lightRequiring", flower.getGrowingTips().getLighting().getLightRequiring());
                growingTips.appendChild(lighting);

                Element watering = doc.createElement("watering");
                watering.setAttribute("measure", flower.getGrowingTips().getWatering().getMeasure());
                watering.setTextContent(flower.getGrowingTips().getWatering().getContent());
                growingTips.appendChild(watering);

                flowerXml.appendChild(growingTips);

                Element multiplying = doc.createElement("multiplying");
                multiplying.setTextContent(flower.getMultiplying());
                flowerXml.appendChild(multiplying);


            }
            /*
            Element root = doc.createElement("studs");
		doc.appendChild(root);
		Element st1 = doc.createElement("stud");
		root.appendChild(st1);

		Element name = doc.createElement("name");
		name.setTextContent(s1.getName());
		st1.appendChild(name);

		Element age = doc.createElement("age");
		age.setTextContent(s1.getAge() + "");
		st1.appendChild(age);

		Element st2 = doc.createElement("stud");
		root.appendChild(st2);

             */

/*
            // добавляем первый дочерний элемент к корневому
            rootElement.appendChild(getLanguage(doc, "1", "Java", "21"));

            //добавляем второй дочерний элемент к корневому
            rootElement.appendChild(getLanguage(doc, "2", "C", "44"));

            //создаем объект TransformerFactory для печати в консоль

 */
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            // для красивого вывода в консоль
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);

            //печатаем в консоль или файл
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File(nameFile));

            //записываем данные
            transformer.transform(source, console);
            transformer.transform(source, file);
            boolean checkXml = checkXMLforXSD("output.dom.xml", "input.xsd");
            System.out.println("checked xml: " + checkXml);
            System.out.println("Создание XML файла закончено");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static boolean checkXMLforXSD (String pathXml, String pathXsd) throws Exception{
        try{
            File xml = new File(pathXml);
            File xsd = new File(pathXsd);
            if(!xml.exists()) System.out.println("Not found xml file");
            if(!xsd.exists()) System.out.println("Not found xsd file");

            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new StreamSource(pathXsd));
            Validator validator = schema.newValidator();
            validator.validate( new StreamSource(pathXml));
            return true;
        }catch(SAXException e){
            System.out.println(e.getMessage());
            return false;
        }

    }

  /*  // метод для создания нового узла XML-файла
    private static Node getLanguage(Document doc, String id, String name, String age) {
        Element language = doc.createElement("Language");

        // устанавливаем атрибут id
        language.setAttribute("id", id);

        // создаем элемент name
        language.appendChild(getLanguageElements(doc, language, "name", name));

        // создаем элемент age
        language.appendChild(getLanguageElements(doc, language, "age", age));
        return language;
    }


    // утилитный метод для создание нового узла XML-файла
    private static Node getLanguageElements(Document doc, Element element, String name, String value) {
        Element node = doc.createElement(name);
        node.appendChild(doc.createTextNode(value));
        return node;
    }

   */

    // PLACE YOUR CODE HERE
//    public static void main(String[] args) {
//        DOMController dmC = new DOMController("input.xml");
//        dmC.readDataXML();
//        dmC.createDataXML();
//    }
}
