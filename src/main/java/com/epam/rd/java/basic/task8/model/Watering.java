package com.epam.rd.java.basic.task8.model;

public class Watering {
    String measure;
    String content;

    public Watering() {
    }

    public Watering(String measure, String content) {
        this.measure = measure;
        this.content = content;
    }

    @Override
    public String toString() {
        return "Watering{" +
                "measure='" + measure + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
