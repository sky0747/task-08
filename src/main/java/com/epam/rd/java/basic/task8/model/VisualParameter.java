package com.epam.rd.java.basic.task8.model;

import java.util.List;

public class VisualParameter {
    String stemColour;
    String leafColour;
    AveLenFlower aveLenFlower;

    public VisualParameter() {
    }

    public VisualParameter(String stemColour, String leafColour, AveLenFlower aveLenFlower) {
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
    }

    @Override
    public String  toString() {
        return "VisualParameter{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AveLenFlower getAveLenFlower() {
        return aveLenFlower;
    }

    public void setAveLenFlower(AveLenFlower aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }
}
