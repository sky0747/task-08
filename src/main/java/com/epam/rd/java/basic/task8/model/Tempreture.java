package com.epam.rd.java.basic.task8.model;

public class Tempreture {
    String measure;
    String content;

    public Tempreture() {
    }

    public Tempreture(String measure, String content) {
        this.measure = measure;
        this.content = content;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Tempreture{" +
                "measure='" + measure + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
